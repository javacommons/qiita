package javacommons.qiita;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class QiitaItemTime {
    public static List<String> getOnePage(String url, int page) throws IOException {
        String revisionUrl = "https://qiita.com" + url + "/revisions";
        Document document = QiitaUtil.getJsoupConnection(revisionUrl)
                .data("page", "" + page)
                .get();
        Elements elements = document.select("time");
        List<String> result = new ArrayList<>();
        for (Element element : elements) {
            String datetime = element.attr("datetime");
            result.add(datetime);
        }
        return result;
    }

    public static String getCreatedTimestamp(String url) {
        try {
            String result = "";
            for (int i = 1; ; i++) {
                List<String> onePage = null;
                onePage = QiitaItemTime.getOnePage(url, i);
                if (onePage.size() == 0) break;
                result = onePage.get(onePage.size() - 1);
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

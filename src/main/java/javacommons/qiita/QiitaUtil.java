package javacommons.qiita;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class QiitaUtil {
    public static String startDate = "2011-09-14";
    public static String userAgent = "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36";

    public static Connection getJsoupConnection(String url) {
        Connection conn = Jsoup.connect(url).userAgent(QiitaUtil.userAgent)
                .header("Accept-Language", "ja-JP");
        return conn;
    }

    public static Date parseTimestamp(String ts) {
        if (ts == null) return null;
        try {
            return new QiitaDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse(ts);
        } catch (ParseException ex1) {
            try {
                return new QiitaDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").parse(ts);
            } catch (ParseException ex2) {
                try {
                    return new QiitaDateFormat("yyyy-MM-dd").parse(ts);
                } catch (ParseException ex3) {
                    return null;
                }
            }
        }
    }

    public static int getDateYear(Date ts) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(ts);
        int year = calendar.get(Calendar.YEAR);
        return year;
    }

    public static String formatDate(Date ts) {
        if (ts == null) return null;
        String result = new QiitaDateFormat("yyyy-MM-dd").format(ts);
        return result;
    }

    public static String formatTimestamp(Date ts) {
        return formatTimestamp(ts, false);
    }

    public static String formatTimestamp(Date ts, boolean longFormat) {
        if (ts == null) return null;
        String pattern = longFormat ? "yyyy-MM-dd'T'HH:mm:ss.SSSXXX" : "yyyy-MM-dd'T'HH:mm:ssXXX";
        String result = new QiitaDateFormat(pattern).format(ts);
        return result;
    }

    public static String formatCurrentTimestamp() {
        return formatCurrentTimestamp(false);
    }

    public static String formatCurrentTimestamp(boolean longFormat) {
        return formatTimestamp(new Date(), longFormat);
    }

    public static String toJson(Object x) {
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(x);
        } catch (JsonProcessingException e) {
            return null;
        }
        return json;
    }
}

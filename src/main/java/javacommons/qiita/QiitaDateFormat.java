package javacommons.qiita;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class QiitaDateFormat extends SimpleDateFormat {
    public QiitaDateFormat(String pattern) {
        super(pattern);
        this.setTimeZone(TimeZone.getTimeZone("Asia/Tokyo"));
    }
}

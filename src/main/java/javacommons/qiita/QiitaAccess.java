package javacommons.qiita;

import com.gitlab.javacommons.paizadb.PaizaDb;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import javacommons.qiitadb.QiitaDb;

import java.sql.SQLException;

public class QiitaAccess {
    private PaizaDb.DbType dbType;

    public QiitaAccess(PaizaDb.DbType dbType) {
        this.dbType = dbType;
    }

    public JdbcConnectionSource getJdbcConnectionSource() throws SQLException {
        JdbcConnectionSource connectionSource = QiitaDb.getJdbcConnectionSource(this.dbType);
        return connectionSource;
    }

    /*
    public void saveToBatch() throws Exception {
        try (JdbcConnectionSource connectionSource = this.getJdbcConnectionSource()) {
            Dao<Item, String> itemDao = DaoManager.createDao(connectionSource, Item.class);
            Dao<Batch, String> batchDao = DaoManager.createDao(connectionSource, Batch.class);
            QueryBuilder<Item, String> qb = itemDao.queryBuilder();
            qb.orderBy("batch", false);
            Optional<Item> first = itemDao.query(qb.prepare()).stream().findFirst();
            if (first.isPresent()) {
                qb = itemDao.queryBuilder();
                qb.where().ne("batch", first.get().batch);
                List<Item> notUpToDate = itemDao.query(qb.prepare());
                if (batchDao.queryForId(first.get().batch) == null) {
                    List<Item> items = itemDao.queryForAll();
                    String json = QiitaUtil.toJson(items);
                    Batch batch = new Batch(first.get().batch);
                    batch.count = notUpToDate.size();
                    batch.json = json;
                    batch.ts = QiitaUtil.formatCurrentTimestamp();
                    batchDao.create(batch);
                }
            }
        }
    }
    */
}
